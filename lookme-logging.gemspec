# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lookme_logging/version'

Gem::Specification.new do |s|
  s.name = 'lookme-logging'
  s.version = LookmeLogging::VERSION
  s.authors = ['chen.zhao']
  s.summary = 'lookme logging'

  s.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  s.require_paths = ['lib']
end