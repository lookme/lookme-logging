# lookme-logging

## 紹介

ログを管理するライブラリです。

## 機能

* 標準出力/標準エラー/ファイルログを対応する
* 既存のログも管理できる、例えば、Rails.logger
* ログの取得は一元で管理する、出力先を変えたい時ソースコード変更なし
* 一回のログリクエストは複数のファイルに出力できる
* Rails用のリクエストIDを入れるフォーマットがある

## 依頼

## 使い方

### Quick Start

    irb > require 'lookme-logging'
     => true
    irb > logger = LookmeLogging::LoggerManager.get_logger('foo')
     => #<Logger:0x007f91eaa57408 @progname=nil, @level=1, @default_formatter=#<Logger::Formatter:0x007f91eaa573e0 @datetime_format=nil>, @formatter=#<Logger::Formatter:0x007f91eaa57480 @datetime_format=nil>, @logdev=#<Logger::LogDevice:0x007f91eaa57390 @shift_size=nil, @shift_age=nil, @filename=nil, @dev=#<IO:<STDOUT>>, @mutex=#<Logger::LogDevice::LogDeviceMutex:0x007f91eaa57340 @mon_owner=nil, @mon_count=0, @mon_mutex=#<Mutex:0x007f91eaa572f0>>>>
    irb > logger.info('foo'){'run'}
    I, [2017-09-05T15:45:53.398055 #3631]  INFO -- foo: run
     => true

デフォルトは標準出力になります。

### logger API

アプリ側では、知るべきAPIは2つだけです。

  LookmeLogging::LoggerManager#get_logger(name: (Symbol|String))
  LookmeLogging::LoggerManager#find_logger(prefix: (Symbol|Class|String))

1つ目はログのキーで特定しログを取ってくるAPIです。もしログは見つからないならデフォルトのログを返ってきます。
最初から何も設定してない場合、デフォルトのログは標準出力になります。
２つ目はクラス目などで一番近いログを取得するAPIです。例えば、クラス `Foo::Bar` で探すと、 `Foo::Bar`, `Foo` の順番でログを探します。名前空間を使ってるクラスには便利な機能です。もしどっちらもないなら、デフォルトのログになります。

まとめてみると、使い方はこんな感じです。

    logger = get logger
    logger.level(key){message}

### 設定

lookme-loggingの特徴としては、ログを一元管理することです。
実際はログを利用する前にログを設定します。

    LookmeLogging::LoggerManager.configure do |repository|
      repository.default_level = :info
      repository.default_formatter = Logger::Formatter.new

      repository.add_logger('foo', Logger.new('foo.log'))
      
      repository.add_std_logger('stdout', :stdout)
      repository.add_std_logger('stderr', :stderr, level: :warn)

      repository.add_file_logger('file1', 'file1.log')
      repository.add_file_logger('file2', 'file2.log', level: :error)

      repository.as_fallback_logger('stdout')
    end

ここではデフォルトのログレベルやフォーマットをセットし、いくつかのログを登録し、最後標準出力をデフォルトのログに設定します。

個別のログを事前に用意し、アプリでそれぞれのログ名で取り出すだけではあまり管理する意味がないので、lookme-loggingではログリダイレクトなどができます。

例えば、この設定

    LookmeLogging::LoggerManager.configure do |repository|
      repository.add_file_logger('feature1', 'file1.log')
      repository.add_file_logger('feature2', 'file2.log')
      repository.add_file_logger('foo', 'file.log')

      repository.redirect('Foo::Feature1', 'feature1')
      repository.redirect('Foo::Feature2', 'feature2')
      repository.redirect('Foo', 'foo')
    end

では、 `Foo::Feature1` は `feature1` ログと同じです。

ログリダイレクトはログの別名と理解してもいいです。

あとAPI `find_logger` の例として、

    LookmeLogging::LoggerManager.find_logger(Foo::Feature1::XXX)
    LookmeLogging::LoggerManager.find_logger(Foo::Feature2::YYY)
    LookmeLogging::LoggerManager.find_logger(Foo::Feature3::ZZZ)

は別々に

    'feature1'
    'feature2'
    'foo'

ログになります。アプリでは基本機能ベースで名前空間を作っていますので、名前空間と合わせてログを自動的に選択するのは機能別でログを出せます。

ログリダイレクトでは、実際複数の出力先や個別のログレベルなどが設定できます。

    LookmeLogging::LoggerManager.configure do |repository|
      repository.add_file_logger('feature1', 'file1.log')
      repository.add_file_logger('feature2', 'file2.log')
      repository.add_file_logger('feature3', 'file3.log', level: :debug)
      repository.add_file_logger('common-error', 'common-error.log', level: :error)

      repository.redirect('Foo::Feature1', ['feature1', 'common-error'])
      repository.redirect('Foo::Feature2', ['feature2', 'common-error'])
      repository.redirect('Foo::Feature3', ['feature3', 'common-error'], level: :info)
    end

この設定で `Foo::Feature1` 名前空間にあるクラスがエラーのレベルで出力すると、ログファイル `file1.log` 、`common-error.log` に同時に出します。
`Foo::Feature3` 名前空間にあるクラスは、DEBUGレベル出力するログメッセージは、本来feature3に出せますが、個別ログの設定で、出せなくなります。ライブラリ的なクラスのログメッセージを出力したくない時は便利です。

### Rails logger

Railsでは、もし複数のログファイルに出力すると、ログメッセージは繋がりにくい場合があります。もしログにリクエストIDがあれば、ログの調査は楽なるはずです。lookme-loggingはログにリクエストIDも出力できるように、rake middlewareとログのフォーマットを用意しました。

使い方は

`application.rb` に

    config.middleware.insert_after 'ActionDispatch::RequestId', 'LookmeLogging::Rails::RequestContextCollector'

を追加し、各環境のファイル（development.rb, production.rb）に

    config.log_formatter = LookmeLogging::Rails::RequestContextLoggerFormatter.new

をセットして、最後lookme-loggingを設定するところ、デフォルトフォーマットを設置すれば完了です。

    LookmeLogging::LoggerManager.configure do |repository|
      repository.default_formatter = LookmeLogging::Rails::RequestContextLoggerFormatter.new
    end

