require 'logger'
require 'set'

require 'lookme_logging/composite_logger'
require 'lookme_logging/lambda_logger_formatter'
require 'lookme_logging/mdc'
require 'lookme_logging/log_context'
require 'lookme_logging/logger_repository'
require 'lookme_logging/logger_repository_builder'
require 'lookme_logging/logger_manager'

require 'lookme_logging/rails/request_context_collector'
require 'lookme_logging/rails/request_context_logger_formatter'

module LookmeLogging
  LEVEL_DEBUG = :debug
  LEVEL_INFO = :info
  LEVEL_WARN = :warn
  LEVEL_ERROR = :error

  LEVELS = ::Set.new([LEVEL_DEBUG, LEVEL_INFO, LEVEL_WARN, LEVEL_ERROR])

  LOGGER_FALLBACK = 'fallback'

  SEPARATOR = '::'
end