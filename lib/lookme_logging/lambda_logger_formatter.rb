module LookmeLogging
  class LambdaLoggerFormatter
    def initialize(lambda)
      @lambda = lambda
    end

    def call(severity, time, prog_name, msg)
      context = LogContext.new(severity, time, prog_name, msg)
      @lambda.call(context)
    end
  end
end