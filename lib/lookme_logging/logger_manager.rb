module LookmeLogging
  class LoggerManager

    @repository = nil

    class << self
      # @param [String] name
      # @return [Logger]
      def get_logger(name)
        repository.find_logger(name.to_s)
      end

      def find_logger(prefix)
        repository.find_logger_by_prefix(prefix.to_s)
      end

      def configure
        builder = LoggerRepositoryBuilder.new
        yield builder
        @repository = builder.build
      end

      private

      # @return [LookmeLogging::LoggerRepository]
      def repository
        @repository ||= LoggerRepositoryBuilder.new.build
      end
    end
  end
end