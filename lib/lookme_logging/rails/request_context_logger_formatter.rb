module LookmeLogging
  module Rails
    class RequestContextLoggerFormatter
      PATTERN = "[%s #%d] %5s -- %s: [%s] %s\n"

      def call(severity, time, prog_name, msg)
        context = LookmeLogging::LogContext.new(severity, time, prog_name, msg)
        PATTERN % [
            context.datetime_formatted,
            context.process_id,
            context.level,
            context.program_name,
            context.mdc('action_dispatch.request_id'),
            context.message2string
        ]
      end
    end
  end
end