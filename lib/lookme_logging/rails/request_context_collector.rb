module LookmeLogging
  module Rails
    class RequestContextCollector
      KEY_REQUEST_ID = 'action_dispatch.request_id'

      def initialize(app)
        @app = app
      end

      def call(env)
        request_id = env['action_dispatch.request_id']
        LookmeLogging::MDC.put(KEY_REQUEST_ID, request_id)
        @app.call(env)
      ensure
        LookmeLogging::MDC.remove(KEY_REQUEST_ID)
      end
    end
  end
end