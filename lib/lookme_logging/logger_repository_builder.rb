module LookmeLogging
  class LoggerRepositoryBuilder
    class LoggerConfigError < ::StandardError
    end

    attr_reader :default_level
    attr_reader :default_formatter

    def initialize
      @loggers = {}
      @default_level = LEVEL_INFO
      @default_formatter = ::Logger::Formatter.new
    end

    def default_level=(default_level)
      unless LEVELS.include?(default_level)
        raise ::ArgumentError, "unexpected log level #{default_level}, available #{LEVELS}"
      end
      @default_level = default_level
    end

    def default_formatter=(default_formatter)
      case default_formatter
        when ::Proc
          @default_formatter = LambdaLoggerFormatter.new(default_formatter)
        else
          @default_formatter = default_formatter
      end
    end

    def add_logger(name, logger)
      @loggers[name] = logger
    end

    def add_std_logger(name, type, options = {})
      case type
        when :stdout
          add_logger(name, setup_ruby_logger(Logger.new(STDOUT), options))
        when :stderr
          add_logger(name, setup_ruby_logger(Logger.new(STDERR), options))
        else
          raise LoggerConfigError, "unexpected std logger type #{type}"
      end
    end

    def add_file_logger(name, path, options = {})
      add_logger(name, setup_ruby_logger(Logger.new(path), options))
    end

    def as_fallback_logger(name)
      if @loggers.include?(name)
        @loggers[LOGGER_FALLBACK] = @loggers[name]
      else
        raise LoggerConfigError, "no such logger #{name}"
      end
    end

    # @param [String] name
    def get_logger(name)
      logger = @loggers[name]
      raise ::ArgumentError, "logger #{name} not found" if logger.nil?
      logger
    end

    # @param [String] from
    # @param [Array<String>] tos
    # @param [Hash] options
    def redirect(from, tos, options = {})
      raise LoggerConfigError, "logger #{from} exists" if @loggers.include?(from)

      loggers = tos.map {|to| @loggers[to]}.compact
      raise LoggerConfigError, "logger not found #{tos}" if loggers.empty?
      level = options[:level] ? map_level(options[:level]) : nil
      add_logger(from, CompositeLogger.new(loggers, level))
    end

    def build
      add_fallback_if_missing
      LoggerRepository.new(@loggers)
    end

    private

    def add_fallback_if_missing
      return if @loggers.include?(LOGGER_FALLBACK)
      add_std_logger(LOGGER_FALLBACK, :stdout)
    end

    # @param [Logger] logger
    # @param [Hash] options
    def setup_ruby_logger(logger, options)
      logger.level = map_level(options[:level] || @default_level)
      logger.formatter = options[:formatter] || @default_formatter
      logger
    end

    def map_level(level)
      case level
        when LEVEL_ERROR
          ::Logger::ERROR
        when LEVEL_WARN
          ::Logger::WARN
        when LEVEL_INFO
          ::Logger::INFO
        when LEVEL_DEBUG
          ::Logger::DEBUG
        else
          raise LoggerConfigError, "unexpected logger level #{level}"
      end
    end
  end
end