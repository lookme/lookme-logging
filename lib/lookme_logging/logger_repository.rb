module LookmeLogging
  class LoggerRepository
    def initialize(loggers)
      @loggers = loggers
    end

    def find_logger(name)
      @loggers[name] || @loggers[LOGGER_FALLBACK]
    end

    def find_logger_by_prefix(prefix)
      parts = prefix.split(SEPARATOR)
      find_logger_by_parts(parts)
    end

    private

    def find_logger_by_parts(parts)
      if parts.empty?
        return @loggers[LOGGER_FALLBACK]
      end

      name = parts.join(SEPARATOR)
      logger = @loggers[name]
      if logger
        logger
      else
        find_logger_by_parts(parts[0, (parts.length - 1)])
      end
    end
  end
end