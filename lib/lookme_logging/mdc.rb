module LookmeLogging
  class MDC
    class << self
      def put(key, value)
        ::Thread.current.thread_variable_set(key, value)
      end

      def get(key)
        ::Thread.current.thread_variable_get(key)
      end

      def remove(key)
        ::Thread.current.thread_variable_set(key, nil)
      end
    end
  end
end