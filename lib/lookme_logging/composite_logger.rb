module LookmeLogging
  class CompositeLogger
    # @param [Array<Logger>] loggers
    # @param [Fixnum] level
    def initialize(loggers, level)
      @loggers = loggers
      @level = level
    end

    def fatal(progname = nil, &block)
      return if @level && ::Logger::FATAL < @level
      @loggers.each {|l| l.fatal(progname, &block)}
    end

    def error(progname = nil, &block)
      return if @level && ::Logger::ERROR < @level
      @loggers.each {|l| l.error(progname, &block)}
    end

    def warn(progname = nil, &block)
      return if @level && ::Logger::WARN < @level
      @loggers.each {|l| l.warn(progname, &block)}
    end

    def info(progname = nil, &block)
      return if @level && ::Logger::INFO < @level
      @loggers.each {|l| l.info(progname, &block)}
    end

    def debug(progname = nil, &block)
      return if @level && ::Logger::DEBUG < @level
      @loggers.each {|l| l.debug(progname, &block)}
    end
  end
end