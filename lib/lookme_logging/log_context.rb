module LookmeLogging
  class LogContext
    attr_reader :level
    attr_reader :datetime
    attr_reader :program_name
    attr_reader :message

    def initialize(level, datetime, program_name, message)
      @level = level
      @datetime = datetime
      @program_name = program_name
      @message = message
    end

    def process_id
      $$
    end

    def datetime_formatted(format = nil)
      if format
        @datetime.strftime(format)
      else
        "#{@datetime.strftime('%Y-%m-%d %H:%M:%S')}.#{'%06d' % @datetime.usec.to_s}"
      end
    end

    # @return [String]
    def message2string
      case @message
        when ::String
          @message
        when ::Exception
          "#{@message.message} (#{@message.class})\n" <<
              (@message.backtrace || []).join("\n")
        else
          @message.inspect
      end
    end

    def mdc(key)
      LookmeLogging::MDC.get(key)
    end
  end
end